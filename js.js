function Hamburger(size, stuffing) {
    try {
        if (size.name === 'small' || size.name === 'large'){
            this._size = size;
        }else{
            throw new HamburgerException('Need size object!')
        }
    } catch(error){
        console.log(error.massage);
    }
    try {
      if (stuffing.name === 'cheese' || stuffing.name === 'potato' || stuffing.name === 'salad'){
          this._stuffing = stuffing;
      }  else {
          throw new HamburgerException('Need stuffing object!')
      }
    } catch (error) {
        console.log(error.massage);
    }
    this._topping = [];
}

Hamburger.SIZE_SMALL = {
    name: 'small',
    type: 'size',
    price: 50,
    calorie: 20,
};
Hamburger.SIZE_LARGE = {
    name: 'large',
    type: 'size',
    price: 100,
    calorie: 40,
};
Hamburger.STUFFING_CHEESE = {
    name: 'cheese',
    type: 'stuffing',
    price: 10,
    calorie: 20,
};
Hamburger.STUFFING_SALAD = {
    name: 'salad',
    type: 'stuffing',
    price: 20,
    calorie: 5,
};
Hamburger.STUFFING_POTATO = {
    name: 'potato',
    type: 'stuffing',
    price: 15,
    calorie: 10,
};
Hamburger.TOPPING_MAYO = {
    name: 'mayo',
    type: 'topping',
    price: 20,
    calorie: 5,
};
Hamburger.TOPPING_SPICE = {
    name: 'spice',
    type: 'topping',
    price: 15,
    calorie: 0,
};

function HamburgerException (massage) {
    this.massage = massage;
}

Hamburger.prototype.addTopping = function (topping) {
    try {
        if (topping.name === 'spice' || topping.name === 'mayo') {
            var isExist = this._topping.find(function(item){
                return topping.name === item.name;
            });
            if (isExist === undefined){
                this._topping.push(topping);
                return this._topping;
            } else {
                return console.log('такая добавка уже есть')
            }
        }
    }catch (err) {
        console.log('Нет такой добавки')
    }
};

Hamburger.prototype.removeTopping = function (topping){
    try {
        var isExist = this._topping.indexOf(topping);
        if (isExist >=0){
            this._topping.splice(isExist,1);
            return this._topping;
        }
    }catch (err) {
        console.log('топпинг не найден')
    }
};

Hamburger.prototype.getToppings = function (){
    console.log(this._topping);
    return this._topping;
};

Hamburger.prototype.getSize = function (){
    console.log(`Этот гамбургер размера ${this._size.name}`);
    return this._size;
};

Hamburger.prototype.getStuffing = function (){
    console.log(this._stuffing);
    return this._stuffing;
};

Hamburger.prototype.calculatePrice = function () {
    var toppingPrice = function(){
        var toppingPrice = 0;
        for (let value of this._topping){
            return toppingPrice += +value.price;
        }
    };
    var totalPrice = 0;
    return totalPrice + this._size.price + this._stuffing.price + toppingPrice.call(this);
};

Hamburger.prototype.calculateCalories = function () {
    var toppingCalories = function(){
        var toppingCalories = 0;
        for (let value of this._topping){
            return toppingCalories += +value.calorie;
        }
    };
    var totalCalories = 0;
    return totalCalories + this._size.calorie + this._stuffing.calorie + toppingCalories.call(this);
};


// маленький гамбургер с начинкой из сыра
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1

// не передали обязательные параметры
var h2 = new Hamburger(); // => HamburgerException: no size given

// передаем некорректные значения, добавку вместо размера
var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
// => HamburgerException: invalid size 'TOPPING_SAUCE'

// добавляем много добавок
var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// HamburgerException: duplicate topping 'TOPPING_MAYO'
